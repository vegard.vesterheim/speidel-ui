#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import sys
import requests

data = {}
try:
    with open("data/api_key.js", "r") as jsonfile:
        data = json.load(jsonfile)
except:
    pass

# Token for Authorization
try:
    api_access_key = sys.argv[1]
except Exception as e:
    api_access_key = data["api_data"]["local_api_key"]

API_URL = "https://api.brewersfriend.com/v1/recipes"

LIMIT = 20
offset = 0


def get_bf_recipes():
    headers = {"X-API-KEY": api_access_key}

    # find out total number of pages
    r = requests.get(url=API_URL, headers=headers)
    if r.status_code == requests.codes.ok:
        content = r.json()
    else:
        r.raise_for_status()

    total_record = int(content["count"])
    # total_record = 10
    print(f"Found {total_record} recipes", file=sys.stderr)

    all_items = []
    querystring = {"ingredients": "1"}

    # loop through all offset and return JSON object
    for offset in range(0, total_record, LIMIT):
        querystring["offset"] = str(offset)
        response = requests.get(
            url=API_URL, headers=headers, params=querystring
        ).json()
        all_items.extend(response["recipes"])
        progress(offset, total_record)

        # print(offset, file=sys.stderr)

    progress(total_record, total_record)
    print("", file=sys.stderr)
    return all_items


def progress(count, total, status=""):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = "=" * filled_len + "-" * (bar_len - filled_len)

    sys.stderr.write("[%s] %s%s ...%s\r" % (bar, percents, "%", status))
    sys.stderr.flush()  # As suggested by Rom Ruben (see: http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console/27871113#comment50529068_27871113)


if __name__ == "__main__":
    all_items = get_bf_recipes()
    bfrecipes = {"recipes": all_items}
    print(
        "(function(exports) { exports.recipes="
        + json.dumps(bfrecipes["recipes"], sort_keys=True, indent=4)
        + "})(this.brewersfriend_data = {});"
    )

    # data = json.dumps(bfrecipes, sort_keys=True, indent=4)
    # print (data)
