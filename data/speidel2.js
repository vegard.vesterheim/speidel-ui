var app = new Vue({
    el: '#speidel_app',
    data: {
        speidel: '',
        recipes: [],
        speidelvisible: 'hidden',
        bfrecipe: null,
        bfrecipes: null,
        api_key: null,
        api_email: null,
        loading_speidel: false,
        recipe_name: 'Nameless',
        mash_times: [30,30,10,0,0],
        mash_temps: [50,66,76,0,0],
        hop_times: [60,15,5,0,0,0],
        hop_days: [7,0],
        fermentation_days: 14,
        boil_time: 60,
	boil_temp: 100,
	grain_temp: null,
        show_manual_form: false,
        show_reminders: false,
        include_snapshots: true,
    },

    watch: {
        recipe_candidates: function () {
            this.populateRecipesfield();
        }
    },
    // FIXME, doesn't work
    computed: {
        recipe_candidates: function () {
            if (! this.bfrecipes || this.bfrecipes.length == 0) {
                return [];
            }
            if (this.include_snapshots) {
                return this.bfrecipes.recipes;
            } else {
                 return this.bfrecipes.recipes.
                    filter(function(x) {return x.snapshot == 0 });
            }
        },
        num_bfrecipes: function () {
            if (this.bfrecipes && 'recipes' in this.bfrecipes) {
                return this.recipe_candidates.length;
            } else {
                return 0;
            }
        },
        recipe_string: function (){
            let recipe_mash_elements = this.mash_times.map(function (e,i) { return [e,app.$data.mash_temps[i]] });
            let mash_steps_string = recipe_mash_elements.map(function(e) {return [e[1],e[0]].join('X')}).join('X');

            let spice_steps_string = this.hop_times.join('X');
            let boil_temp = this.boil_temp;
            if (typeof boil_temp === 'undefined' || boil_temp == 0) {
                boil_temp = 100;
            }
            return (
                [9,
                 Math.round(this.grain_temp || this.mash_temps[0]),
                 mash_steps_string,
                 Math.round(this.boil_time),
                 Math.round(this.boil_temp),
                 spice_steps_string].
                    join('X')+'.'+this.recipe_name
            );
        },
        ical_string: function () {
            let icsMSG = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//Our Company//NONSGML v1.0//EN\n";
            // iCAL format: 19980119T070000Z
            let epoch = (new Date).valueOf();
            let fermentation_days = 14; // FIXME
            let hop_days = this.hop_days.filter(function(x) {if (x > 0) {return (x)} });
            let dts = hop_days.map(function(x) {return (epoch + (Number(fermentation_days) - x)*(60*60*24*1000))});
            let dts_dates = dts.map(function (x) {
                let d=new Date(x);
                return (d.getYear()+1900+
                        speidellib.sprintf('%02d',d.getMonth()+1)+
                        speidellib.sprintf('%02d',d.getDate())+
                        'T'+
                        speidellib.sprintf('%02d',d.getUTCHours())+
                        speidellib.sprintf('%02d',d.getMinutes())+
                        '00Z')});
            let vevents = dts_dates.map(function (x, index)
                                        {
                                            return "BEGIN:VEVENT\n" +
                                                "UID:hoptimes-" + app.$data.recipe_name + index +
                                                "\nDTSTAMP:"+ x +
                                                "\nDTSTART:" + x +
                                                "\nDTEND:" + x +
                                                "\nSUMMARY:Dry Hop time\nEND:VEVENT"});
            let ferm_end_date = (new Date(epoch+(this.fermentation_days*(60*60*24*1000))));
            let fermentation_end_ics = (ferm_end_date.getYear()+1900+
                                        speidellib.sprintf('%02d',ferm_end_date.getMonth()+1)+
                                        speidellib.sprintf('%02d',ferm_end_date.getDate())+
                                        'T'+
                                        speidellib.sprintf('%02d',ferm_end_date.getUTCHours())+
                                        speidellib.sprintf('%02d',ferm_end_date.getMinutes())+
                                        '00Z');
            let fermentation_event = "BEGIN:VEVENT\n" +
                "UID:ferm_finish-" + app.$data.recipe_name +
                "\nDTSTAMP:"+ fermentation_end_ics +
                "\nDTSTART:" + fermentation_end_ics +
                "\nDTEND:" + fermentation_end_ics +
                "\nSUMMARY:Time to bottle\nEND:VEVENT";
            icsMSG = icsMSG + vevents.join('\n') + "\n" + fermentation_event + "\nEND:VCALENDAR";
            // console.log ("ICS: \n"+icsMSG);
            return "data:text/calendar;charset=utf8,"+escape(icsMSG);
            // iCAL format: 19980119T070000Z
        }

    },
    created(){
        if (typeof api_data !== 'undefined') {
            this.api_key   = api_data.local_api_key;
            this.api_email = api_data.local_api_email;
            this.api_url   = api_data.local_api_url;
        }
        if (typeof brewersfriend_data !== 'undefined') {
            this.bfrecipes = brewersfriend_data;
        }
        this.speidel = this.query_args('speidel');
        console.log("Speidel is: "+this.speidel)
        if (this.speidel) {
            this.loadRecipes();
        }
    },
    mounted(){
    },
    methods: {
        populateRecipesfield: function () {
            let app = this;
            $("#bfname").autocomplete({
                source: this.recipe_candidates.
                    map(function(x) {return {label: x.title+' ('+x.updated_at.substring(0,10)+')', value: x}}),
                select: function( event, ui ) {
                    $("#bfname").val(ui.item.value.title);
                    app.$data.bfrecipe = ui.item.value; // FIXME
                    app.show_manual_form = true;
                    app.BFRecipe_to_form();
                    return false;
                },
                focus: function( event, ui ) {
                    $("#bfname").val(ui.item.value.title);
                    return false;
                }
            });
        },
        uploadFormRecipe: function (data) {
            console.log("Uploading form recipe: "+this.recipe_string);
            speidellib.sendrz
            (
                this.speidel,
                this.recipe_string,
                9,
                this.populateRecipesdata,
                this.speidelNotfound,
            );

            //9X50X50X30X66X30X76X10X0X0X0X0X60X100X60X15X5X0X0.HveteIPA
            //9X50X50X30X66X30X76X10X0X0X0X0X60X100X60X15X5X0X0X0.HveteIPA
            //9X50X50X30X66X30X76X10X0X0X0X0X60X100X60X15X5X0X0X0

        },
        BFRecipe_to_form: function (data) {
            let spice_additions = [0,0,0,0,0,0];
            let mash_steps_temps = [0,0,0,0,0];
            let mash_steps_times = [0,0,0,0,0];
            let recipe_no = 9; // FIXME
            // console.log("Uploadbfrecipe: "+this.bfrecipe);
            let recipe = app.$data.bfrecipe;
            if (recipe && ! recipe.mashsteps) {
                alert ("No mash steps found in recope, invalid recipe!");
                return null;
            }
            let recipe_mash_steps = recipe.mashsteps.
                filter(function(x) {
                    return (x.mashtype == 'Sparge' ||
                            x.mashtype == 'Infusion' ||
                            x.mashtype == 'Strike' ||
                            x.mashtype == 'Temperature')});
            let recipe_mash_temps = recipe_mash_steps.
                    map(function(x){return Math.round(x.temp)});
            let boil_temp = recipe.boiltemp;
            if (typeof boil_temp === 'undefined' || boil_temp == 0) {
                boil_temp = 100;
            }
            let grain_temp = recipe.grain_temp || recipe_mash_temps[0];
            let mashin_temp = recipe_mash_temps[0];
            let recipe_mash_times = recipe_mash_steps.map(function(x){return (x.mashtime ? x.mashtime : 0)});
            recipe_mash_times = recipe_mash_times.concat(mash_steps_times).slice(0,5);
            recipe_mash_temps = recipe_mash_temps.concat(mash_steps_temps).slice(0,5);


            let recipe_mash_elements = recipe_mash_times.map(function (e,i) { return [e,recipe_mash_temps[i]] });
            let mash_steps_string = recipe_mash_elements.map(function(e) {return [e[1],e[0]].join('X')}).join('X')

            let hop_times = recipe.hops.
                filter(function(x) {return x.hopuse == 'Boil' ||
                                    x.hopuse == 'Aroma'}).
                map(function (x) {return x.hoptime});
            let dryhop_times = recipe.hops.
                filter(function(x) {return x.hopuse.toLowerCase() == 'dry hop'}).
                    map(function (x) {return x.hoptime});

            let other_times = recipe.others.
                    filter(function(x) {return x.otheruse == 'Boil' || x.otheruse == 'Whirlpool'}).
                    map(function (x) {return x.othertime});
            hop_times = hop_times.concat(other_times);
            let spice_steps_unique = hop_times.filter(function(value, index, self) { //array unique
                return self.indexOf(value) === index;
            }).sort(function(a,b) {return a-b}).reverse();
            let spice_steps_string = spice_steps_unique.concat(spice_additions).slice(0,6).join('X');

            let name = recipe.title;
            let boil_time=recipe.boiltime || 60;
            let recipe_name = recipe.title;
            let recipe_string =
                    [recipe_no,
                     Math.round(grain_temp),
                     mash_steps_string,
                     Math.round(boil_time),
                     Math.round(boil_temp),
                     spice_steps_string].
                    join('X')+'.'+recipe_name;
            console.log ("Recipe-string: "+recipe_string);
            this.recipe_to_form(speidellib.parse_recipe(recipe_string));
            this.show_manual_form = true;
            // speidellib.sendrz
            // (
            //     this.speidel,
            //     recipe_string,
            //     9,
            //     this.populateRecipesdata,
            //     this.speidelNotfound,
            //);


        },
        populateRecipesdata: function (data) {
            this.recipes = data;
            this.speidelvisible = 'visible';
            this.loading_speidel = false;

        },
        genLabel: function (data) {
            let url = 'http://localhost:5000/beer-label.cgi?'
            let recipe = app.$data.bfrecipe;
            url = url + 'name=' + encodeURIComponent(recipe.title);
            url = url + '&title_size=0';
            url = url + '&barcode_url=' + encodeURIComponent(recipe.recipeViewUrl);
            console.log ('genlabel, uri:' + url);
            document.getElementById('label').src = url;
        },

        // Read existing recipes from Speidel
        loadRecipes: function () {
            this.loading_speidel = true;
            speidellib.getrz
            (
                this.speidel,
                this.populateRecipesdata,
                this.speidelNotfound
            );

        },

        // When Speidel recipe clicked on, populate edit form
        speidelRecipeSelected: function (event) {
            let id = event.currentTarget.id;
            this.recipe_to_form(this.recipes[id]);
        },

        // Delete recipe from Speidel
        removeRecipe: function (event) {
            speidellib.delrz
            (
                this.speidel,
                event.currentTarget.id,
                this.populateRecipesdata,
                this.nofunc
            );
            console.log ("remove "+event.currentTarget.id);
        },
        speidelNotfound: function (error, obj) {
            this.loading_speidel = false;
            alert ("No response from Speidel, try again and/or doublecheck IP/Name");
            this.speidelvisible = 'hidden';
        },
        nofunc: function () {
        },

        // BeerXML file dropped on edit area
        edit_area_drop_handler: function (ev) {
            console.log("recipe dropped on me");
            ev.stopPropagation();
            ev.preventDefault();
            let vue = this;
            // If dropped items aren't files, reject them
            var dt = ev.dataTransfer;
            //console.log ("Files: "+dt.files)
            //console.log ("Items: "+dt.items)
            if (dt.files) {
                // Use DataTransfer interface to access the file(s)
                for (var i=0; i < dt.files.length; i++) {
                    var file = dt.files[i];
                    var reader = new FileReader();
                    reader.onload = (function(e) {
                        var contents = e.target.result;
                        // console.log ("Got contents:" + contents);
                        var recipes = Brauhaus.Recipe.fromBeerXml(contents);
                        console.log ("recipes: "+recipes);
                        var recipe_string = speidellib.beerxml2speidel(recipes);
                        if (recipe_string) {
                            console.log("Dropped recipe_string: "+recipe_string);
                            recipe = speidellib.parse_recipe(recipe_string);
                            vue.recipe_to_form(recipe);
                            console.log("Dropped recipe: "+recipe);
                        }

                    });
                    reader.readAsText(file);
                    console.log("... file[" + i + "].name = " + file.name);
                }
            }
            vue.show_manual_form = true;
        },
        recipe_to_form: function (recipe) {
            this.show_manual_form = true;
            this.recipe_name = recipe.name;
            this.grain_temp = recipe.einmaischTemp;
            for (var i=0; i < recipe.rast.length; i++) {
                this.mash_temps[i] = recipe.rast[i].temp;
                this.mash_times[i] = recipe.rast[i].time;
            }
            this.boil_time = recipe.hopfengaben.duration;
            this.boil_temp =  recipe.hopfengaben.temp;
            let hop_times = recipe.hopfengaben.gaben;
            for (var i=0; i < hop_times.length; i++) {
                this.hop_times[i] = hop_times[i];
            }
        },
        drop_handler: function (ev) {
            console.log("Drop");
            console.log(ev);
            ev.stopPropagation();
            ev.preventDefault();
            let vue = this;
            // If dropped items aren't files, reject them
            var dt = ev.dataTransfer;
            //console.log ("Files: "+dt.files)
            //console.log ("Items: "+dt.items)
            if (dt.files) {
                // Use DataTransfer interface to access the file(s)
                for (var i=0; i < dt.files.length; i++) {
                    var file = dt.files[i];
                    var reader = new FileReader();
                    reader.onload = (function(e) {
                        var contents = e.target.result;
                        // console.log ("Got contents:" + contents);
                        var recipes = Brauhaus.Recipe.fromBeerXml(contents);
                        console.log ("recipes: "+recipes);
                        var recipe_string = speidellib.beerxml2speidel(recipes);
                        if (recipe_string) {
                            speidellib.sendrz
                            (
                                vue.speidel,
                                recipe_string,
                                9,
                                vue.populateRecipesdata,
                                vue.nofunc,
                            );
                        }
                        // await sleep(2000);

                    });
                    reader.readAsText(file);
                    console.log("... file[" + i + "].name = " + file.name);
                }
            }
        },
        submit_ical: function () {
            location.href = this.ical_string;
        },
        query_args: function (key) {
            key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
            var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
            return match && decodeURIComponent(match[1].replace(/\+/g, " "));
        }
    }
})
