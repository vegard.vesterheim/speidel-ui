// https://eloquentjavascript.net/10_modules.html
(function(exports) {
    exports.getrecipes = function (api_url, api_key, api_email, returndata, failure) {
        var data = new FormData();
        data.append('api_key', api_key);
        data.append('api_email', api_email);
        data.append('ingredients', 1);
        $.ajax({
            url:api_url,
            headers: {'X-API-KEY': api_key},
            //cache:false,
            crossDomain:true,
            method:'GET',
            type: 'GET',
            processData: false,
            contentType: false,
            async:true,
            timeout:30000,
            beforeSend:function(obj){
	        console.log("Before GET ("+api_url+"): "+ new Date());
            }
        }).done(function(data){
            if (returndata) {
                returndata(data);
            }
        }).fail(function(obj,textStat,error){
            $('upload_status').text('Upload FAILED!');
            console.log("Error date: "+ (new Date()));
            console.log("Error: "+textStat);
            console.log("Error Object: "+error);
            console.log("Object: "+obj);
            if (failure) {
                failure(obj, error);
            }
        });
    }
    exports.getlocalrecipes = function (file, returndata, failure) {
        $.getJSON(file, returndata);
    }


})(this.brewersfriend = {});
