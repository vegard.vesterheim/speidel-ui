// Generated by CoffeeScript 1.9.3
(function() {
  Brauhaus.Recipe.prototype.toBeerXml = function() {
    var fermentable, hop, i, j, k, l, len, len1, len2, len3, len4, m, misc, ref, ref1, ref2, ref3, ref4, step, xml, yeast;
    xml = '<?xml version="1.0" encoding="utf-8"?><recipes><recipe>';
    xml += '<version>1</version>';
    xml += "<name>" + this.name + "</name>";
    xml += "<brewer>" + this.author + "</brewer>";
    xml += "<batch_size>" + this.batchSize + "</batch_size>";
    xml += "<boil_size>" + this.boilSize + "</boil_size>";
    xml += "<efficiency>" + this.mashEfficiency + "</efficiency>";
    if (this.primaryDays) {
      xml += "<primary_age>" + this.primaryDays + "</primary_age>";
    }
    if (this.primaryTemp) {
      xml += "<primary_temp>" + this.primaryTemp + "</primary_temp>";
    }
    if (this.secondaryDays) {
      xml += "<secondary_age>" + this.secondaryDays + "</secondary_age>";
    }
    if (this.secondaryTemp) {
      xml += "<secondary_temp>" + this.secondaryTemp + "</secondary_temp>";
    }
    if (this.tertiaryDays) {
      xml += "<tertiary_age>" + this.tertiaryDays + "</tertiary_age>";
    }
    if (this.tertiaryTemp) {
      xml += "<tertiary_temp>" + this.tertiaryTemp + "</tertiary_temp>";
    }
    if (this.agingDays) {
      xml += "<age>" + this.agingDays + "</age>";
    }
    if (this.agingTemp) {
      xml += "<age_temp>" + this.agingTemp + "</age_temp>";
    }
    if (this.bottlingTemp) {
      xml += "<carbonation_temp>" + this.bottlingTemp + "</carbonation_temp>";
    }
    if (this.bottlingPressure) {
      xml += "<carbonation>" + this.bottlingPressure + "</carbonation>";
    }
    if (this.style) {
      xml += '<style><version>1</version>';
      if (this.style.name) {
        xml += "<name>" + this.style.name + "</name>";
      }
      if (this.style.category) {
        xml += "<category>" + this.style.category + "</category>";
      }
      xml += "<og_min>" + this.style.og[0] + "</og_min><og_max>" + this.style.og[1] + "</og_max>";
      xml += "<fg_min>" + this.style.fg[0] + "</fg_min><fg_max>" + this.style.fg[1] + "</fg_max>";
      xml += "<ibu_min>" + this.style.ibu[0] + "</ibu_min><ibu_max>" + this.style.ibu[1] + "</ibu_max>";
      xml += "<color_min>" + this.style.color[0] + "</color_min><color_max>" + this.style.color[1] + "</color_max>";
      xml += "<abv_min>" + this.style.abv[0] + "</abv_min><abv_max>" + this.style.abv[1] + "</abv_max>";
      xml += "<carb_min>" + this.style.carb[0] + "</carb_min><carb_max>" + this.style.carb[1] + "</carb_max>";
      xml += '</style>';
    }
    xml += '<fermentables>';
    ref = this.fermentables;
    for (i = 0, len = ref.length; i < len; i++) {
      fermentable = ref[i];
      xml += '<fermentable><version>1</version>';
      xml += "<name>" + fermentable.name + "</name>";
      xml += "<type>" + (fermentable.type()) + "</type>";
      xml += "<weight>" + (fermentable.weight.toFixed(1)) + "</weight>";
      xml += "<yield>" + (fermentable["yield"].toFixed(1)) + "</yield>";
      xml += "<color>" + (fermentable.color.toFixed(1)) + "</color>";
      xml += '</fermentable>';
    }
    xml += '</fermentables>';
    xml += '<hops>';
    ref1 = this.spices.filter(function(item) {
      return item.aa > 0;
    });
    for (j = 0, len1 = ref1.length; j < len1; j++) {
      hop = ref1[j];
      xml += '<hop><version>1</version>';
      xml += "<name>" + hop.name + "</name>";
      xml += "<time>" + hop.time + "</time>";
      xml += "<amount>" + hop.weight + "</amount>";
      xml += "<alpha>" + (hop.aa.toFixed(2)) + "</alpha>";
      xml += "<use>" + hop.use + "</use>";
      xml += "<form>" + hop.form + "</form>";
      xml += '</hop>';
    }
    xml += '</hops>';
    xml += '<yeasts>';
    ref2 = this.yeast;
    for (k = 0, len2 = ref2.length; k < len2; k++) {
      yeast = ref2[k];
      xml += '<yeast><version>1</version>';
      xml += "<name>" + yeast.name + "</name>";
      xml += "<type>" + yeast.type + "</type>";
      xml += "<form>" + yeast.form + "</form>";
      xml += "<attenuation>" + yeast.attenuation + "</attenuation>";
      xml += '</yeast>';
    }
    xml += '</yeasts>';
    xml += '<miscs>';
    ref3 = this.spices.filter(function(item) {
      return item.aa === 0;
    });
    for (l = 0, len3 = ref3.length; l < len3; l++) {
      misc = ref3[l];
      xml += '<misc><version>1</version>';
      xml += "<name>" + misc.name + "</name>";
      xml += "<time>" + misc.time + "</time>";
      xml += "<amount>" + misc.weight + "</amount>";
      xml += "<use>" + misc.use + "</use>";
      xml += '</misc>';
    }
    xml += '</miscs>';
    if (this.mash) {
      xml += '<mash><version>1</version>';
      xml += "<name>" + this.mash.name + "</name>";
      xml += "<grain_temp>" + this.mash.grainTemp + "</grain_temp>";
      xml += "<sparge_temp>" + this.mash.spargeTemp + "</sparge_temp>";
      xml += "<ph>" + this.mash.ph + "</ph>";
      xml += "<notes>" + this.mash.notes + "</notes>";
      xml += '<mash_steps>';
      ref4 = this.mash.steps;
      for (m = 0, len4 = ref4.length; m < len4; m++) {
        step = ref4[m];
        xml += '<mash_step><version>1</version>';
        xml += "<name>" + step.name + "</name>";
        xml += "<description>" + (step.description(true, this.grainWeight())) + "</description>";
        xml += "<step_time>" + step.time + "</step_time>";
        xml += "<step_temp>" + step.temp + "</step_temp>";
        xml += "<end_temp>" + step.endTemp + "</end_temp>";
        xml += "<ramp_time>" + step.rampTime + "</ramp_time>";
        if (step.type === 'Decoction') {
          xml += "<decoction_amt>" + (step.waterRatio * this.grainWeight()) + "</decoction_amt>";
        } else {
          xml += "<infuse_amount>" + (step.waterRatio * this.grainWeight()) + "</infuse_amount>";
        }
        xml += '</mash_step>';
      }
      xml += '</mash_steps>';
      xml += '</mash>';
    }
    return xml += '</recipe></recipes>';
  };

}).call(this);
